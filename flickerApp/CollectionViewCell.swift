//
//  CollectionViewCell.swift
//  flickerApp
//
//  Created by Canyon Duncan on 10/18/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var imageTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.lightGray
    }
    
}

//
//  ViewController.swift
//  flickerApp
//
//  Created by Canyon Duncan on 10/18/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    var selectedRow = Int()
    var flickrResults = [[String: AnyObject]]()
    var flickrPhotos = [FlickrPhoto]()
    @IBOutlet weak var searchBox: UITextField!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = UIColor.darkGray
        collectionView.reloadData()
        collectionView.dataSource = self
        collectionView.delegate = self
        searchBox.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //----------------//
    //FLICKER URL
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }
    //----------------//
   
    
    //CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.flickrResults.count
    }
    
    //CollectionView
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.imageView.backgroundColor = UIColor.white
        if(self.flickrPhotos.count > 0){
            
            let flickrPhoto = self.flickrPhotos[indexPath.row]
            DispatchQueue.global().async {
                guard let url = flickrPhoto.flickrImageURL(),
                    let imageData = try? Data(contentsOf: url as URL) else {
                        return
                }
            
            if let image = UIImage(data: imageData) {
                flickrPhoto.thumbnail = image
                self.flickrPhotos.append(flickrPhoto)
            }
            //cell.imageView.image = self.flickrPhotos[indexPath.row].thumbnail
            
            }
            cell.imageTitle.text = flickrPhoto.title
            cell.imageView.image = flickrPhoto.thumbnail
        }
        
        return cell
    }
    //CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 20, height: self.view.frame.size.width/2 - 20)
    }
    //-----------------//
    
    
    //DISMISS KEYBOARD
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks "Return" key  on the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        flickrResults.removeAll()
        flickrPhotos.removeAll()
        
        let url = flickrSearchURLForSearchTerm(self.searchBox.text!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
            //print(json)
            
            let photos = json["photos"] as! Dictionary<String, Any>
            self.flickrResults = (photos["photo"] as? [[String: AnyObject]])!
            for photo in self.flickrResults {
                guard let photoID = photo["id"] as? String,
                    let farm = photo["farm"] as? Int ,
                    let server = photo["server"] as? String ,
                    let secret = photo["secret"] as? String,
                    let title = photo["title"] as? String
                    else {
                        break
                }
                let flickrPhoto = FlickrPhoto(photoID: photoID, farm: farm, server: server, secret: secret, title: title)
                self.flickrPhotos.append(flickrPhoto)
                
            }
            //print(self.flickrPhotos.)
            //print(self.flickrResults)
            //            DispatchQueue.global().async {
            //                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
             self.collectionView.reloadData()
            }
            //            }
            
            
            
        }
        
        task.resume()
        
        
        return true
    }
    //-----------------//
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedRow = indexPath.row
        performSegue(withIdentifier: "showViewImage", sender: Any?.self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showViewImage"){
            if let nextViewController = segue.destination as? ViewImage{
                nextViewController.flickrPhotos.append(self.flickrPhotos[self.selectedRow])
            }
        }
    }
    
   

    
    func flickrImageURL(_ farm:String, server:String, photoID:String, secret:String, size:String = "m") -> URL? {
        if let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size).jpg") {
            return url
        }
        return nil
    }
    
    

}


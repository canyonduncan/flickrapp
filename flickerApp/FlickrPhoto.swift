//
//  FlickrPhoto.swift
//  flickerApp
//
//  Created by Canyon Duncan on 10/20/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import Foundation
import UIKit

class FlickrPhoto {
    var thumbnail : UIImage?
    let photoID : String
    let farm : Int
    let server : String
    let secret : String
    let title : String
    
    init (photoID:String,farm:Int, server:String, secret:String, title:String) {
        self.photoID = photoID
        self.farm = farm
        self.server = server
        self.secret = secret
        self.title = title
    }
    
    func flickrImageURL(_ size:String = "m") -> URL? {
        if let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size).jpg") {
            return url
        }
        return nil
    }
    
    
    
    
    
}



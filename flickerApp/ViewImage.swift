//
//  ViewImage.swift
//  flickerApp
//
//  Created by Canyon Duncan on 10/18/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class ViewImage: UIViewController, UIScrollViewDelegate {

    var flickrPhotos = [FlickrPhoto]()
    
    @IBOutlet weak var flickrTitle: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var flickrImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let photo = flickrPhotos[0]
        //var photo = flickrPhotos[0].thumbnail
        self.flickrImage.image  = photo.thumbnail
        self.flickrTitle.text = photo.title
        self.flickrTitle.sizeToFit()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return flickrImage
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
